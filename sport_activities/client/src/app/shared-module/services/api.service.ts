import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ApiSchema, applyApiSchema, reverseApiSchema } from '../../utils/api-schema';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';

interface RequestInfo<T> {
  reqBody?: any;
  reqOptions?: HttpRequestOptionsArgs<T>;
  schema?: ApiSchema<T>;
}

export interface HttpRequestOptionsArgs<T> {
  headers?: HttpHeaders | {
    [header: string]: string | string[];
  };
  observe?: 'body';
  params?: HttpParams | {
    [param: string]: string | string[];
  };
  reportProgress?: boolean;
  withCredentials?: boolean;

  // Extended Args
  schema?: ApiSchema<T>;
  bodyReverseSchema?: ApiSchema<any>;
}

export interface BlobInfo {
  blob: Blob;
  fileName?: string;
}

@Injectable()
export class ApiService {
  constructor(private http: HttpClient) {
  }

  get<T>(path: string,
         options?: HttpRequestOptionsArgs<T>): Observable<T> {
    const { schema, reqOptions } = this.getRequestInfo(options);
    return this.http.get<T>(path, reqOptions).pipe(
      map(response => applyApiSchema(response, schema))
    );
  }

  getMany<T>(path: string,
             options?: HttpRequestOptionsArgs<T>): Observable<T[]> {
    return this.get<T>(path, options) as any as Observable<T[]>;
  }

  post<T>(path: string, body: any, options?: HttpRequestOptionsArgs<T>): Observable<T> {
    const { schema, reqOptions, reqBody } = this.getRequestInfo(options, body);
    return this.http.post<T>(path, reqBody, reqOptions).pipe(
      map(response => applyApiSchema(response, schema))
    );
  }

  private getRequestInfo<T>(options: HttpRequestOptionsArgs<T>, body?: any): RequestInfo<T> {
    if (!options) {
      return { reqBody: body };
    }

    const requestInfo: RequestInfo<T> = { reqOptions: { ...options } };
    delete requestInfo.reqOptions.schema;
    delete requestInfo.reqOptions.bodyReverseSchema;
    requestInfo.reqBody = options.bodyReverseSchema ? reverseApiSchema(body, options.bodyReverseSchema) : body;
    requestInfo.schema = options.schema;
    return requestInfo;
  }
}
