import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-dialog',
  template: `
    <h1 mat-dialog-title>Hey You!</h1>
    <div mat-dialog-content>
      <p>Enter you email address to register</p>
      <mat-form-field>
        <input matInput [(ngModel)]="data.result">
      </mat-form-field>
    </div>
    <div mat-dialog-actions>
      <button mat-button (click)="onNoClick()">No Thanks</button>
      <button mat-button [mat-dialog-close]="data.result" cdkFocusInitial>Register</button>
    </div>
  `
})
export class AppDialogComponent {

  constructor(public dialogRef: MatDialogRef<AppDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
