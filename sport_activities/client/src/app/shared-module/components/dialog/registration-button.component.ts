import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AppDialogComponent } from './dialog.component';

@Component({
  selector: 'app-registration-button',
  template: `
    <button mat-raised-button
            (click)="openDialog()">
      REGISTER
    </button>
  `
})
export class RegistrationButton {
  @Input() activityName: string;
  @Output() register: EventEmitter<string> = new EventEmitter();
  result: string;

  constructor(public dialog: MatDialog) {
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(AppDialogComponent,
      {
        width: '250px',
        data: { result: this.result, activity: this.activityName }
      });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          this.register.emit(result);
        }
      });
  }
}
