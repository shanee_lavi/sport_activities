import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  styleUrls: ['./list.component.scss'],
  template: `
    <mat-list>
      <mat-list-item *ngFor="let item of items">
        <!--<img matListAvatar src="..." alt="...">-->
        <h3 matLine> {{ item }} </h3>
        <!--<p matLine>-->
        <!--<span> {{participant.email}} </span>-->
        <!--<span class="demo-2"> &#45;&#45; {{message.content}} </span>-->
        <!--</p>-->
      </mat-list-item>
    </mat-list>
  `
})
export class ListComponent implements OnInit {
  @Input() items: string[];

  constructor() {
  }

  ngOnInit() {
  }

}
