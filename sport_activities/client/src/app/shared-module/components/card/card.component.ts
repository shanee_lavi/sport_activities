import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  styleUrls: ['./card.component.scss'],
  template: `
    <mat-card class="example-card">

      <mat-card-header>
        <div mat-card-avatar class="example-header-image"></div>
        <mat-card-title>{{ title }}</mat-card-title>
        <mat-card-subtitle>{{ subtitle }}</mat-card-subtitle>
      </mat-card-header>

      <img mat-card-image
           [src]="'../../../../assets/images/' + title +'.jpg'"
           [alt]="'Photo of' + title">

      <mat-card-content>
        <app-list [items]="items"></app-list>
      </mat-card-content>

      <!--<mat-card-actions>-->
      <!--<button mat-button>{{ buttonText }}</button>-->
      <!--</mat-card-actions>-->

      <app-registration-button [activityName]="title"
                               (register)="onRegister($event)">
        loading
      </app-registration-button>

    </mat-card>
  `
})
export class CardComponent {
  @Input() title: string;
  @Input() subtitle: string;
  @Input() items: string[];
  @Input() buttonText: string;
  @Output() register: EventEmitter<string> = new EventEmitter();

  onRegister(event: string) {
    debugger;
    this.register.emit(event)
  }
}

