import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  styleUrls: ['./navbar.component.scss'],
  template: `
    <mat-toolbar [color]="'primary'">
      <mat-toolbar-row>
        <span>TLV Activities</span>
        <span class="example-spacer"></span>
        <mat-icon class="example-icon">favorite</mat-icon>
        <mat-icon class="example-icon">delete</mat-icon>
      </mat-toolbar-row>
    </mat-toolbar>
  `
})
export class NavbarComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
