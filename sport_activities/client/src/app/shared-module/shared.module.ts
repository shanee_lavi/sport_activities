import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card/card.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { MaterialModule } from '../material-module/material.module';
import { ListComponent } from './components/list/list.component';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './services/api.service';
import { RegistrationButton } from './components/dialog/registration-button.component';
import { AppDialogComponent } from './components/dialog/dialog.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    HttpClientModule,
    FormsModule
  ],
  exports: [
    CardComponent,
    NavbarComponent,
  ],
  declarations: [
    CardComponent,
    NavbarComponent,
    ListComponent,
    RegistrationButton,
    AppDialogComponent
  ],
  bootstrap: [
    AppDialogComponent
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        ApiService
      ]
    }
  }
}
