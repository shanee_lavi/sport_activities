import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.scss'],
  template: `
    <header>
      <app-navbar></app-navbar>
    </header>
    <main>
      <router-outlet></router-outlet>
    </main>
    <footer>
    </footer>
  `
})
export class AppComponent implements OnInit {

  // constructor(apiService: ApiService) {
  // }

  ngOnInit(): void {
    // apiService.get()
  }
}
