export type ApiSchema<T> = {
  [P in keyof T]?: ApiRule | ApiRule[];
  };

interface ApiRule {
  runRule(obj, key): void;

  reverseRule(obj, key): void;
}

class DateApiRule implements ApiRule {
  runRule(obj: any, key: string) {
    obj[key] = obj[key] && new Date(obj[key]);
  }

  reverseRule(obj: any, key: string) {
    obj[key] = obj[key] && (obj[key] as Date).toISOString();
  }
}

export const dateApiRule = new DateApiRule();

export class ReplaceKeyApiRule implements ApiRule {
  constructor(public readonly serverKey: string) {
  }

  runRule(obj, key) {
    obj[key] = obj[this.serverKey];
    delete obj[this.serverKey];
  }

  reverseRule(obj, key) {
    obj[this.serverKey] = obj[key];
    delete obj[key];
  }
}

export class SubSchemaApiRule<T> implements ApiRule {
  constructor(private schema: ApiSchema<T>, public readonly isDictionary = false) {
  }

  runRule(obj, key) {
    applyApiSchema(obj[key], this.schema, this.isDictionary);
  }

  reverseRule(obj, key) {
    obj[key] = reverseApiSchema(obj[key], this.schema, this.isDictionary);
  }
}

export function reverseApiSchema<T>(obj: T, schema: ApiSchema<T>, isDictionary = false): any {
  if (!schema || !obj) {
    return obj;
  }
  let reverseObj: any;
  if (Array.isArray(obj)) {
    reverseObj = obj.map(item => reverseApiSchema(item, schema)) as any;
  } else if (isDictionary) {
    reverseObj = {};
    Object.keys(obj)
      .forEach(childObjKey => {
        reverseObj[childObjKey] = reverseApiSchema(obj[childObjKey], schema);
      });
  } else {
    // Avoid mutating the original object as we don't want to change it when making a post request.
    reverseObj = { ...obj as any };
    Object.keys(schema).forEach(key => {
      const schemaRule = schema[key as any];
      const schemaRules = Array.isArray(schemaRule) ? [...schemaRule].reverse() : [schemaRule];
      schemaRules.forEach(rule => rule.reverseRule(reverseObj, key));
    });
  }
  return reverseObj;
}

export function applyApiSchema<T>(obj: any, schema: ApiSchema<T>, isDictionary = false): T {
  if (!schema || !obj) {
    return obj;
  }

  if (Array.isArray(obj)) {
    obj.forEach(item => applyApiSchema(item, schema));
  } else if (isDictionary) {
    Object.keys(obj)
      .forEach(childObjKey => applyApiSchema(obj[childObjKey], schema));
  } else {
    Object.keys(schema).forEach(key => {
      const schemaRule = schema[key as any];
      const schemaRules = Array.isArray(schemaRule) ? schemaRule : [schemaRule];
      schemaRules.forEach(rule => rule.runRule(obj, key));
    });
  }
  return obj;
}

export function getServerColumn(clientField: string, dataSchema: ApiSchema<any>): any {
  let serverField = clientField;
  const apiRule = dataSchema[clientField];
  if (apiRule) {
    if (apiRule instanceof ReplaceKeyApiRule) {
      serverField = apiRule.serverKey;
    } else if (Array.isArray(apiRule)) {
      const replaceRule = apiRule.find(rule => rule instanceof ReplaceKeyApiRule);
      if (replaceRule) {
        serverField = replaceRule.serverKey;
      }
    }
  }
  return serverField;
}
