export interface Activity {
  name: string;
  minRegistered: number;
  maxRegistered: number;
  day: number;
  month: number;
  location: string;
  price: number;
  timesThisMonth: number;
  isActive: boolean;
  users: string[];
}

export interface Time {
  minute: number;
  hour: number;
}

