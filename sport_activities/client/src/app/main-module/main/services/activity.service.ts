import { Injectable } from '@angular/core';
import { ApiService } from '../../../shared-module/services/api.service';
import { Observable } from 'rxjs/Observable';
import { Activity } from '../../../api-models/activity';
import 'rxjs/add/observable/of';
import { activitySchema } from './activity.schema';

@Injectable()
export class ActivityService {

  // private path = 'http://10.196.160.118:8080/api';
  private path = 'http://localhost/api';

  // private path = '/api';

  constructor(private apiService: ApiService) {
  }

  getActivities(): Observable<Activity[]> {
    return this.apiService.getMany(`${this.path}/current_status`, { schema: activitySchema });
    // return Observable.of(activities);
  }

  registerToActivity(participantName: string,
                     activityName: string,
                     weekDay: number): Observable<boolean> {
    // return Observable.of(true);
    const body = {
      name: participantName,
      activity_name: activityName,
      weekday: weekDay
    };
    return this.apiService.post(`${this.path}/register`, body)
  }
}

export const activities: Activity[] = [
  {
    name: 'soccer',
    minRegistered: 15,
    maxRegistered: 20,
    day: 2,
    month: 5,
    location: 'Goal Time',
    price: 55,
    timesThisMonth: 2,
    isActive: true,
    users: [
      'akochavi@paloaltonetworks.com',
      'damar@paloaltonetworks.com'
    ]
  },
  {
    name: 'pilates',
    minRegistered: 3,
    maxRegistered: 10,
    day: 4,
    month: 5,
    location: 'Fun Room',
    price: 55,
    timesThisMonth: 4,
    isActive: true,
    users: [
      'akochavi@paloaltonetworks.com',
      'damar@paloaltonetworks.com'
    ]
  },
  {
    name: 'band',
    minRegistered: 3,
    maxRegistered: 7,
    day: 1,
    month: 5,
    location: 'Rehearsal Room',
    price: 55,
    timesThisMonth: 2,
    isActive: true,
    users: [
      'akochavi@paloaltonetworks.com',
      'damar@paloaltonetworks.com'
    ]
  }
];
