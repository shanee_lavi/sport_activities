import { ApiSchema, ReplaceKeyApiRule } from '../../../utils/api-schema';
import { Activity } from '../../../api-models/activity';

export const activitySchema: ApiSchema<Activity> = {
  maxRegistered: new ReplaceKeyApiRule('max_registered'),
  minRegistered: new ReplaceKeyApiRule('min_registered'),
};
