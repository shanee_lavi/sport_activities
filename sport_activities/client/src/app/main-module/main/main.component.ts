import { Component, OnDestroy, OnInit } from '@angular/core';
import { Activity } from '../../api-models/activity';
import { ActivityService } from './services/activity.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-main',
  styleUrls: ['./main.component.scss'],
  template: `
    <app-card *ngFor="let activity of activities"
              [title]="activity.name"
              [subtitle]="'Week day: ' + activity.day"
              [items]="activity.users"
              [buttonText]="'REGISTER'"
              (register)="onRegister($event, activity)">
    </app-card>
  `
})
export class MainComponent implements OnInit, OnDestroy {
  public activities: Activity[];
  private activitySubscription: Subscription;

  constructor(private activityService: ActivityService) {
  }

  ngOnInit() {
    this.activitySubscription = this.activityService.getActivities()
      .subscribe((activities: Activity[]) => {
        debugger;
        this.activities = activities;
      });
  }

  ngOnDestroy() {
    this.activitySubscription.unsubscribe();
  }

  onRegister(participant: string, activity: Activity) {
    this.activityService.registerToActivity(participant, activity.name, activity.day)
  }
}
