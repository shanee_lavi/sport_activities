import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { mainRoutingModule } from './main-routing.module';
import { SharedModule } from '../shared-module/shared.module';
import { ActivityService } from './main/services/activity.service';

@NgModule({
  imports: [
    CommonModule,
    mainRoutingModule,
    SharedModule
  ],
  declarations: [
    MainComponent,
  ],
  providers: [
    ActivityService
  ]
})
export class MainModule {
}
