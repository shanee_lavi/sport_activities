from bson import ObjectId
from lc_utilities.types.data_records import DataRecord, IntField, BoolField, StringField, DateTimeField, EnumField, ListField, Field, DataField
from lc_utilities.types.enums import strings_enum

ActivityStatus = strings_enum('ActivityStatus', ('Pending', 'Open', 'Closed'))
RegistrationStatus = strings_enum('RegistrationStatus', ('Pending', 'Registered', 'Open', 'Expired'))


def object_id_from_string(value):
    if isinstance(value, basestring):
        return ObjectId(value)
    return value


class Document(DataRecord):
    _id = Field(ObjectId, required=False, default=None, client_visible=False, load_func=object_id_from_string, dump_func=lambda value: str(value))

    def to_doc(self):
        doc = self.to_dict()
        if doc['_id'] is None:
            doc.pop('_id', None)
        return doc


class Time(DataRecord):
    hour = IntField(max=24)
    minute = IntField(max=60)


def load_time(val):
    if isinstance(val, Time):
        return val

    if isinstance(val, basestring):
        split_time = val.split(':')
        return Time(hour=int(split_time[0]),
                    minute=int(split_time[1]))
    return Time(val)


class Registration(DataRecord):
    name = StringField()
    registration_time = DateTimeField()
    status = EnumField(RegistrationStatus, required=False, default=RegistrationStatus.Pending)


class ActivityTemplate(Document):
    name = StringField()
    weekday = IntField()
    min_registered = IntField()
    max_registered = IntField()
    time = DataField(Time, load_func=load_time)
    duration = IntField()
    location = StringField()
    price = IntField()


class Activity(ActivityTemplate):
    date = DateTimeField()
    status = EnumField(ActivityStatus, required=False, default=ActivityStatus.Pending)
    registrations = ListField(Registration, required=False)


# Responses:

class Response(DataRecord):
    error = StringField(required=False)


class RegistrationResponse(Response):
    available_slots = IntField(required=False, min=0)


class CloseActivity(DataRecord):
    name = StringField()
    weekday = IntField()
    dates = ListField(DateTimeField())
    users_in = ListField(basestring)
    users_out = ListField(basestring)

