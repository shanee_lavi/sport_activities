from __future__ import print_function
from __future__ import unicode_literals

import os
import sys
import csv
import logging
import dateutil.parser

from datetime import datetime
from collections import defaultdict

from rtmbot.core import Plugin, Job
from collections import namedtuple

sys.path.append(os.path.join(os.path.dirname(__file__)))
sys.path.append(os.path.join(os.path.dirname(__file__), "../../../"))

from sport_activities.records import ActivityStatus
from sport_activities.activities import get_open_activity_names, get_current_status, get_registered_users_to_activities, open_registration, \
    add_all_activities_from_template, delete_activity, update_activity, close_registration
from sport_activities.registrations import register, un_register
from sport_activities.calendar_utils import WEEK_DAYS
from sport_activities.mongo_connection import init_mongo


init_mongo()

MAIL_SUFFIX = '@paloaltonetworks.com'

ALL_CHANNEL = 'CAR880V5H'  #CAG8G9141
RUNNER_USER = '<@UAREDR4FL>'

SUPPORT_USER = 'U6GV04NGZ'
ADMIN_USERS = ['U9JG1AJF5', # Uria
               'UAK26MFHQ', # Neta
               'U6PLWTQ2X', # Roy
               'U7C4W0UJU', # Adi
               SUPPORT_USER]


Command = namedtuple('Command', ('usage', 'min_args', 'max_args'))

COMMANDS_USER = {'register':                Command('<activity name> <weekday: 1-5>. Example: register pilates 2', 2, 2),
                 'un_register':             Command('<activity name> <weekday: 1-5>. Example: un_register pilates 2', 2, 2),
                 'status':                  Command('[Optional: activity name]. Example: status pilates', 0, 1)}

COMMANDS_ADMIN = {'generate_monthly_activities':  Command('<year yyyy> <month: 1-12>', 2, 2),
                  'update_activity':        Command('<activity name> <date dd-mm-yyyy> <new_date dd-mm-yyyy>', 3, 3),
                  'delete_activity':        Command('<activity name> <date dd-mm-yyyy>', 2, 2),
                  'open_registration_test': Command('', 0, 0),
                  'open_registration':      Command('<expiration: dd-mm-yyyy>. Example: open_registration 15-06-2018', 1, 1),
                  'close_registration':     Command('', 0, 0),
                  'remind':                 Command('', 0, 0)}


BROAD_NOTIFICATION_COMMANDS = ('open_registration', 'close_registration', 'remind')


def get_available_commands(is_admin):
    return (COMMANDS_ADMIN.keys() if is_admin else []) + COMMANDS_USER.keys()


def get_usage_message(is_admin):
    return 'Usage: `<command> [optional: arguments]`.\n Example: `register HIIT 2`  \nAvailable commands: %s' % ' | '.join(['`%s`' % c for c in get_available_commands(is_admin)])


class RunnerPlugin(Plugin):
    def process_message(self, data):
        if data.get('subtype') in ('channel_join','channel_leave', 'channel_archive'):
            return

        if not data['text'].startswith(RUNNER_USER):
            print('Ignoring %s' % (str(data)))
            return

        is_admin = data['user'] in ADMIN_USERS
        # Take @runner <command> <arg 1> <arg 2> first remove @runner and then split into [<command>, <arg 1> <arg 2>]
        text_no_runner = data['text'].strip().split(' ', 1)[1]

        message_body = text_no_runner.split()
        print("message_body: %s" % message_body)

        command = message_body[0].lower()

        if command == 'uploaded':
            return

        if command == 'Usage' or command not in get_available_commands(is_admin):
            self.outputs.append([data['channel'],  get_usage_message(is_admin)])
            return

        if command in COMMANDS_ADMIN and not is_admin:
            self.outputs.append([data['channel'], 'Sorry, command `{command}` available only for administrator'.format(command=command)])
            return

        command_cookbook = COMMANDS_ADMIN.get(command) or COMMANDS_USER.get(command)
        arguments = message_body[1:] if message_body else []

        if len(arguments) < command_cookbook.min_args or len(arguments) > command_cookbook.max_args:
            self.outputs.append([data['channel'], 'Usage: `%s %s`' % (command, command_cookbook.usage)])
            return

        self.run_command_switch_case(data, command, arguments)

    def run_command_switch_case(self, data, command, arguments):
        try:
            print('Calling command %s' % command)
            if command == 'register':
                self.handle_registration(data, arguments, register_to_activity=True)

            elif command == 'un_register':
                self.handle_registration(data, arguments, register_to_activity=False)

            elif command == 'status':
                self.handle_activity_status(data, arguments)

            elif command == 'generate_monthly_activities':
                self.handle_all_activities(data, arguments)

            elif command == 'update_activity':
                self.handle_update_activity(data, arguments)

            elif command == 'delete_activity':
                self.handle_delete_activity(data, arguments)

            elif command == 'open_registration_test':
                self.handle_open_registration_test(data)

            elif command == 'open_registration':
                self.handle_open_registration(data, arguments)

            elif command == 'close_registration':
                self.handle_close_registration(data)

            elif command == 'remind':
                self.outputs.append([ALL_CHANNEL, '<!here> Activities registration for next month will end soon :scream: Don\'t forget to register :muscle:'])
                return

        except Exception as e:
            logging.exception('Exception during execution')
            self.outputs.append(
                [data['channel'], 'Ooops. That failed.\n```{err_msg}```\nCC <@{support_user}>'.format(err_msg=str(e), support_user=SUPPORT_USER)])

    def catch_all(self, data):
        pass

    def get_username_from_id(self, data):
        return self.slack_client.api_call('users.info', user=data['user'])['user']['name'] + MAIL_SUFFIX

    def get_name_to_id_mapping(self):
        return {user['name']: user['id'] for user in self.slack_client.api_call('users.list')['members']}

    def handle_registration(self, data, arguments, register_to_activity=True):
        validation_error, activity, day = self.handle_register_helper(arguments)

        if validation_error:
            self.outputs.append([data['channel'], validation_error])
            return

        result = (register if register_to_activity else un_register)(self.get_username_from_id(data), activity, day)

        if register_to_activity:
            message = result.error if result.error else 'You have successfully registered to %s on %ss. There %s %d available slots. An update will be sent when registration window closes.' %\
                                                        (activity, WEEK_DAYS[day], 'are' if result.available_slots != 1 else 'is', result.available_slots)
        else:
            message = 'You have successfully unregistered from %s on %ss' % (activity, WEEK_DAYS[day])

        self.outputs.append([data['channel'], message])
        return

    def handle_register_helper(self, arguments):
        activity = arguments[0]
        invalid_activity_message = get_invalid_activity_message(arguments[0])
        if invalid_activity_message:
            return invalid_activity_message, '', ''

        invalid_day_message = 'Invalid day `%s`, please enter a number between 1 and 5' % arguments[1]

        try:
            day = int(arguments[1])
            if not (1 <= day <= 5):
                return invalid_day_message, '', ''

        except Exception:
            return invalid_day_message, '', ''

        return '', activity, day

    def handle_activity_status(self, data, arguments):
        activity = None

        if arguments:
            activity = arguments[0]
            invalid_activity_message = get_invalid_activity_message(activity)
            if invalid_activity_message:
                self.outputs.append([data['channel'], invalid_activity_message])
                return

        status_list = get_current_status()
        message = '\n\n\n'.join([get_activity_status_display(status) for status in status_list if not activity or status['name'] == activity])

        if not status_list:
            message = 'There are no open activities to register to'

        self.outputs.append([data['channel'], message])
        return

    def handle_all_activities(self, data, arguments):
        validation_error, year, month = self.handle_all_activities_helper(arguments)

        if validation_error:
            self.outputs.append([data['channel'], validation_error])
            return

        add_all_activities_from_template(year, month)

        self.outputs.append([data['channel'], "You have successfully added all activities"])
        return

    @staticmethod
    def handle_all_activities_helper(arguments):
        current_year = datetime.now().year
        invalid_year_message = 'Invalid year `%s`, please enter a number bigger than %s' % (arguments[0], current_year - 1)

        try:
            year = int(arguments[0])
            if not current_year <= year:
                return invalid_year_message, '', ''

        except Exception:
            return invalid_year_message, '', ''

        invalid_month_message = 'Invalid month `%s`, please enter a number between 1 and 12' % arguments[1]

        try:
            month = int(arguments[1])
            if not (1 <= month <= 12):
                return invalid_month_message, '', ''

        except Exception:
            return invalid_month_message, '', ''

        return '', year, month

    @staticmethod
    def handle_activity_helper(arguments):
        activity = arguments[0]
        invalid_activity_message = get_invalid_activity_message(activity)

        if invalid_activity_message:
            return invalid_activity_message, '', ''

        try:
            date = dateutil.parser.parse(arguments[1])
        except ValueError:
            return 'Invalid date for expiration. Please use DD-MM-YYYY', '', ''

        return '', activity, date

    def handle_delete_activity(self, data, arguments):
        validation_error, activity, date = self.handle_activity_helper(arguments)

        if validation_error:
            self.outputs.append([data['channel'], validation_error])
            return

        delete_activity(activity, date)
        self.outputs.append([data['channel'], 'Deleting activity: %s, date: %s', activity, date])

    def handle_update_activity(self, data, arguments):
        validation_error, activity, date = self.handle_activity_helper(arguments)

        if validation_error:
            self.outputs.append([data['channel'], validation_error])
            return

        try:
            new_date = dateutil.parser.parse(arguments[2])
        except ValueError:
            self.outputs.append([data['channel'], 'Invalid new date for update. Please use DD-MM-YYYY'])
            return

        update_activity(activity, date, {'date': new_date})
        self.outputs.append([data['channel'], 'Updating activity: %s, date: %s to the date: %s' % (activity, date.date(), new_date.date())])

    def handle_open_registration_test(self, data):
        self.outputs.append([data['channel'], get_open_activities_display(ActivityStatus.Pending)])

    def handle_open_registration(self, data, arguments):
        try:
            expiration = dateutil.parser.parse(arguments[0])
        except ValueError:
            self.outputs.append([data['channel'], 'Invalid date for expiration. Please use DD-MM-YYYY'])
            return

        open_registration(expiration)

        self.outputs.append([ALL_CHANNEL, get_open_activities_display()])

    def handle_close_registration(self, data):
        close_record = close_registration()

        mapping = self.get_name_to_id_mapping()

        for activity in close_record:
            dates = [time.strftime('%d/%m/%Y') for time in activity.dates]
            for user in activity.users_in:
                user_id = mapping[user.split('@')[0]]
                self.slack_client.api_call('chat.postMessage', channel=user_id, text="Confirmation: you have successfully registered to %s on %ss :thumbsup:.\nDates: %s" %
                                    (activity.name, WEEK_DAYS[activity.weekday], ", ".join(dates)))

            for user in activity.users_out:
                user_id = mapping[user.split('@')[0]]
                self.slack_client.api_call('chat.postMessage', channel=user_id,
                                           text="Rejection: unfortunately %s on %ss is fully booked :disappointed:." % (activity.name, WEEK_DAYS[activity.weekday]))

        # closed message
                self.slack_client.api_call('chat.postMessage', channel=ALL_CHANNEL, text="'<!here> Activity registration window is closed")

        # Create and send Csv
        users_to_activities = get_registered_users_to_activities()
        with open('activities_registration.csv', 'w') as csvfile:
            fieldnames = ['Email', 'Amount', 'Activities']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for user_row in users_to_activities:
                writer.writerow({'Email': user_row['username'],
                                 'Amount': len(user_row['activities'])})
#                                 'Activities': user_row['activities']})

        self.slack_client.api_call('files.upload', channels=SUPPORT_USER, filename='activities_registration.csv',
                                  file=open('activities_registration.csv', 'rb'), filetype='csv')


def get_invalid_activity_message(activity):
    available_activities = get_open_activity_names()

    if activity not in available_activities:
        return 'Invalid activity `%s`, please select one of: `%s`' % (activity, ', '.join(available_activities))


def get_activity_status_display(activity_dict):
    title = '%s (%s)' % (activity_dict['name'], WEEK_DAYS[activity_dict['weekday']])
    result = [title, '=' * len(title)]

    registered = [r['name'] for r in activity_dict['registrations']]
    num_registered = len(registered)

    if activity_dict['min_registered'] and num_registered < activity_dict['min_registered']:
        result.append('Still missing %d participants to reach the minimum' % (activity_dict['min_registered'] - num_registered))

    elif activity_dict['max_registered'] and num_registered < activity_dict['max_registered']:
        result.append('Only %s slots available' % (activity_dict['max_registered'] - num_registered))

    if num_registered:
        result.append('The following users have already registered:\n%s' % ('\n'.join(['- %s' % user for user in registered])))
    else:
        result.append('No one has registered yet :disappointed:')

    return '\n'.join(result)


def get_open_activities_display(status=ActivityStatus.Open):
    activities = get_current_status(status)

    result = ['<!here> Registration for next month activities is now open!\n']

    summary = defaultdict(list)

    for activity in activities:
        minimum = activity['min_registered']
        minimum_string = ' (minimum of %s participants)' % minimum
        summary[activity['name']].append('*%s*: %s at %s%s. Dates: %s' %
                                         (activity['name'],
                                          WEEK_DAYS[activity['weekday']],
                                          format_time(activity['time']),
                                          minimum_string,
                                          format_dates_to_days(activity['dates'])))

    for activity_name in summary:
        result += summary[activity_name]

    return '\n\n'.join(result + [':woman-running: :man-running:'])


def format_time(time_dict):
    hour = time_dict['hour']
    minutes = time_dict['minute']

    def pad(input):
        return '0' if input < 10 else ''

    return '%s%s:%s%s' % (pad(hour), hour, pad(minutes), minutes)


def format_dates_to_days(dates):
    days = [d.day for d in dates]
    days_str = ', '.join(str(d) for d in days[:-1]) + ' & %s' % days[-1]

    month = dates[0].month

    return '%s.%s' % (days_str, month)
