import logging
from datetime import datetime

from pyramid.renderers import JSON

from lc_utilities.types.json_utils import dumps_json, base_default, internal_default

from lc_utilities.types.data_records import PartialDataRecord

log = logging.getLogger(__name__)


def webui_compatible_default(obj):
    if isinstance(obj, datetime):
        return obj.isoformat() + 'Z'  # UTC
    elif isinstance(obj, PartialDataRecord):
        return obj.to_json(only_client_visible=True)
    else:
        return base_default(obj)


def register_json_renderer(config, make_default):
    json_renderer = JSON(serializer=_json_serialize)
    config.add_renderer('json', json_renderer)
    if make_default:
        config.add_renderer(None, json_renderer)


def _json_serialize(value, default):
    request = default(RequestCapturer())
    log.debug("Client accepts %s", request.accept)
    return dumps_json(value, indent=None, default=internal_default)


class RequestCapturer(object):
    def __json__(self, request):
        return request
