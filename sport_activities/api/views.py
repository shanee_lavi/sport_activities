import os
import json

from pyramid.view import view_config

from slackclient import SlackClient


def send_slack_notification(channel, text):
    slack_token = os.environ["SLACK_API_TOKEN"]
    sc = SlackClient(slack_token)

    sc.api_call('chat.postMessage', channel=channel, text=text)


from sport_activities.registrations import register, un_register
from sport_activities.activities import get_current_status, is_registration_open


@view_config(route_name='register', request_method='POST')
def register_post_view(request):
    body = json.loads(request.body)
    response = register(body['name'], body['activity_name'], body['weekday'])
    return response


@view_config(route_name='un_register', request_method='POST')
def un_register_post_view(request):
    body = json.loads(request.body)
    response = un_register(body['name'], body['activity_name'], body['weekday'])
    return response


@view_config(route_name='current_status', request_method='GET')
def current_status_get_view(request):
    return get_current_status()


@view_config(route_name='is_registration_open', request_method='GET')
def is_registration_open_get_view(request):
    return is_registration_open()
