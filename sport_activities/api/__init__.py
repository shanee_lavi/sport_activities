from pyramid.config import Configurator
from pyramid.response import Response

from lc_utilities import thread_dump

from sport_activities.mongo_connection import init_mongo
from sport_activities.pyramid_json_utils import register_json_renderer


def test_view(request):
    return Response('<body><h1>Welcome</h1></body>')


def create_app_configuration(**settings):
    return Configurator(settings=settings)


def main(global_config, **settings):
    init_mongo()

    config = create_app_configuration(**settings)

    register_json_renderer(config, True)

    config.add_route('test_page', 'test')
    config.add_view(test_view, route_name='test_page')

    config.add_route('register', 'register')
    config.add_route('un_register', 'un_register')
    config.add_route('current_status', 'current_status')
    config.add_route('is_registration_open', 'is_registration_open')

    config.scan(ignore='.tests')

    thread_dump.register_signal()

    return config.make_wsgi_app()

