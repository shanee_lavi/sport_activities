from pymongo import MongoClient

client = None
db = None


def get_db():
    return db


def init_mongo():
    global client
    global db
    client = MongoClient()
    db = client.activities

