import logging

from bson.son import SON

from sport_activities.mongo_connection import get_db
from sport_activities.records import Activity, ActivityTemplate, ActivityStatus, Time, Response, CloseActivity
from sport_activities.calendar_utils import get_dates_per_weekday


def get_activity_dates(year, month, weekday, activity_time):
    dates = get_dates_per_weekday(year, month, weekday)
    for date in dates:
        date.replace(hour=activity_time.hour, minute=activity_time.minute)
    return dates


def generate_activities_from_template(template, year, month):
    dates = get_activity_dates(year, month, template['weekday'], Time(template['time']))
    template.pop('_id')
    return [Activity(date=date, **template) for date in dates]


def add_all_activities_from_template(year, month):
    templates = list(get_db().activity_template.find())
    if not templates:
        logging.info('No templates defined - no activities were created')
        return

    new_activities = []
    for template in templates:
        get_db().activities.delete_many({'name': template['name'], 'weekday': template['weekday']})
        new_activities.extend(generate_activities_from_template(template, year, month))

    logging.info('Creating %s new activities from %s templates', len(new_activities), len(templates))
    get_db().activities.insert_many([activity.to_doc() for activity in new_activities])


def _validate_overrides(overrides):
    if not all(key in ('date', 'duration', 'location') for key in overrides):
        raise ValueError('Invalid kwargs %s' % overrides)


def add_activity(name, date, overrides=None):
    overrides = overrides or {}
    _validate_overrides(overrides)
    template = get_db().activity_template.find_one({'name': name})
    if template is None:
        return Response(error='No template for activity: %s' % name)

    template.update(overrides)
    activity = Activity(date=date, **template)

    logging.info('Creating new activity: %s, date: %s (overrides: %s)', activity, date, overrides)
    get_db().activities.insert_one(activity.to_doc())


def update_activity(name, date, overrides):
    _validate_overrides(overrides)
    logging.info('Updating activity: %s, date: %s - %s', name, date, overrides)
    get_db().activities.update_one({'name': name, 'date': date}, {'$set': overrides})


def delete_activity(name, date):
    logging.info('Deleting activity: %s, date: %s', name, date)
    get_db().activities.delete_one({'name': name, 'date': date})


def add_activity_template(**kwargs):
    new_activity_template = ActivityTemplate(kwargs)

    logging.info('Adding new activity type: %s', new_activity_template)
    get_db().activity_template.insert_one(new_activity_template.to_doc())


def get_current_status(status=ActivityStatus.Open):
    return list(get_db().activities.aggregate([
        {
            '$match': {'status': status}
        },
        {
            '$group': {'_id': {'name': '$name', 'weekday': '$weekday'},
                       'dates': {'$push': '$date'},
                       'time': {'$first': '$time'},
                       'min_registered': {'$first': '$min_registered'},
                       'max_registered': {'$first': '$max_registered'},
                       'registrations': {'$first': '$registrations'}}
        },
        {
            '$addFields': {'name': '$_id.name', 'weekday': '$_id.weekday'}
        },
        {
            '$project': {'_id': 0}
        }
    ]))


def get_registered_users_to_activities():
    return list(get_db().activities.aggregate([
        {
            '$match': {'status': ActivityStatus.Closed}
        },
        {
            '$group': {'_id': {'name': '$name', 'weekday': '$weekday'},
                       'registrations': {'$first': '$registrations'}}
        },
        {
            '$unwind': '$registrations'
        },
        {
            '$group': {'_id': '$registrations.name',
                       'activities': {'$push': {'name': '$name', 'weekday': '$weekday'}}}
        },
        {
            '$project': {'_id': 0, 'username': '$_id', 'activities': 1}
        }
    ]))


def get_open_activities():
    return list({(a['name'], a['weekday']) for a in
                 get_db().activities.find({'status': ActivityStatus.Open}, {'name': 1, 'weekday': 1, '_id': 0})})


def get_open_activity_names():
    return list({a[0] for a in get_open_activities()})


def open_registration(expiration_time):
    get_db().activities.update_many({'status': 'Pending'}, {'$set': {'status': 'Open'}})
    get_db().registration_expiration.insert_one({'time': expiration_time})


def close_registration():
    delete_res = get_db().registration_expiration.delete_one({})
    if delete_res.deleted_count == 0:
        return None

    get_db().activities.update_many({'status': ActivityStatus.Open}, {'$set': {'status': ActivityStatus.Closed}})
    res = list(get_db().activities.aggregate([
        {'$match': {'status': ActivityStatus.Closed}},
        {'$group': {'_id': {'name': '$name', 'weekday': '$weekday'},
                    'dates': {'$push': '$date'},
                    'registrations': {'$first': '$registrations'},
                    'max_registered': {'$first': '$max_registered'}}},
        {'$unwind': {'path': '$registrations', 'includeArrayIndex': 'reg_idx'}},
        {'$addFields': {'is_registered': {'$cond': [{'$lte': ['$reg_idx', {'$subtract': ['$max_registered', 1]}]}, True, False]}}},
        {'$group': {'_id': {'name': '$_id.name', 'weekday': '$_id.weekday', 'is_registered': '$is_registered'},
                    'users': {'$push': '$registrations.name'},
                    'dates': {'$first': '$dates'}}},
        {'$group': {'_id': {'name': '$_id.name', 'weekday': '$_id.weekday'},
                    'users': {'$push': {'is_registered': '$_id.is_registered', 'list': '$users'}},
                    'dates': {'$first': '$dates'}}},
        {'$project': {'_id': 0, 'name': '$_id.name', 'weekday': '$_id.weekday', 'users': 1, 'dates': 1}}

    ]))

    # some ugliness:
    closures = []
    for a in res:
        users_in = []
        users_out = []
        for u in a['users']:
            if u['is_registered']:
                users_in = u['list']
            else:
                users_out = u['list']
        closures.append(CloseActivity(name=a['name'], weekday=a['weekday'], dates=a['dates'], users_in=users_in, users_out=users_out))

    for closure in closures:
        get_db().activities.update_many({'name': closure.name, 'weekday': closure.weekday, 'status': ActivityStatus.Closed},
                                        {'$pull': {'registrations': {'name': {'$in': closure.users_out}}}})
        get_db().activities.update_many({'name': closure.name,
                                         'weekday': closure.weekday,
                                         'status': ActivityStatus.Closed},
                                        {'$set': {'registrations.$[].status': 'Registered'}})

    return closures


    # Invite API
    # Mail to Uria


def is_registration_open():
    return True if get_db().registration_expiration.count() > 0 else False
