import datetime

ACTIVITY_EXAMPLE = dict(name='',
                        min_registered=5,
                        max_registered=10,
                        day=1,
                        month=1,
                        location='digger',
                        price=55,
                        times_this_month=4,
                        active=True)

REGISTRATION = dict(email='',
                    activity_id='',
                    status=0, #change to enum and default is pending, registered  
                    register_date=datetime.datetime.utcnow())
