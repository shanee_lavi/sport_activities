import logging

from datetime import datetime

from sport_activities.activities import get_open_activities
from sport_activities.calendar_utils import WEEK_DAYS
from sport_activities.mongo_connection import get_db
from sport_activities.records import Registration, Activity, RegistrationResponse, ActivityStatus


def get_user_activity_filter(name, activity_name, weekday):
    return {'name': name, 'activity': activity_name, 'weekday': weekday}


def _format_activity(activity_name, weekday):
    return '%s (%s)' % (activity_name, WEEK_DAYS[weekday])


def register(name, activity_name, weekday):
    new_registration = Registration(name=name, registration_time=datetime.utcnow())

    update_res = get_db().activities.update_many({'name': activity_name,
                                                  'weekday': weekday,
                                                  'status': ActivityStatus.Open,
                                                  'registrations.name': {'$ne': name}},
                                                 {'$push': {'registrations': {'$each': [new_registration.to_dict()],
                                                                              '$sort': {'registration_time': 1}}}})
    if update_res.modified_count == 0:
        open_activities = get_open_activities()
        activity_id = (activity_name, weekday)
        if activity_id not in open_activities:
            resp = RegistrationResponse(error='No open activity: %s. All open activities: %s' %
                                              (_format_activity(*activity_id), ', '.join([_format_activity(*a) for a in open_activities])))
        else:
            resp = RegistrationResponse(error='Already registered to activity: %s' % _format_activity(activity_name, weekday))

        logging.warn('Could not register: %s', resp)
        return resp

    activity = Activity(get_db().activities.find_one({'name': activity_name, 'weekday': weekday}))
    resp = RegistrationResponse(available_slots=max(activity.max_registered - len(activity.registrations), 0))
    logging.info('Successfully registered user: %s to activity: %s, weekday: %s. response: %s', name, activity_name, weekday, resp)
    return resp


def un_register(name, activity_name, weekday):
    res = get_db().activities.update_many({'name': activity_name,
                                           'weekday': weekday,
                                           'status': ActivityStatus.Open,
                                           'registrations.name': name},
                                          {'$pull': {'registrations': {'name': name}}})
    if res.modified_count > 0:
        logging.info('Un-registering user: %s from activity: %s, weekday: %s)', name, activity_name, weekday)
    else:
        logging.warn('User: %s was not registered to activity: %s, weekday: %s', name, activity_name, weekday)
