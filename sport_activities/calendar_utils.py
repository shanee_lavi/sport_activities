import calendar
from datetime import datetime

DAYS_OFFSET = 2
DAYS_PER_WEEK = 7

WEEK_DAYS = {1: 'Sunday',
             2: 'Monday',
             3: 'Tuesday',
             4: 'Wednesday',
             5: 'Thursday',
             6: 'Friday',
             7: 'Saturday'}


def get_dates_per_weekday(year, month, weekday):
    month_cal = calendar.monthcalendar(year, month)
    real_weekday = (weekday - DAYS_OFFSET) % DAYS_PER_WEEK
    return [datetime(year, month, week[real_weekday]) for week in month_cal if week[real_weekday] != 0]
