#!/usr/bin/env bash

#!/bin/bash -e
set -o pipefail

echo "Starting TLV activities"
export TLV_ACT_PATH=/home/lc/sport_activities
export TLV_ACT_LOG=${TLV_ACT_PATH}/tlv_act.log
export TLV_ACT_SCRIPT=${TLV_ACT_PATH}/sport_activities/tlv_activities.py
export PYTHONPATH=.:${TLV_ACT_PATH}

cd ${TLV_ACT_PATH}/sport_activities
exec pserve production.ini >> ${TLV_ACT_LOG} 2>&1
echo "TLV activities started"