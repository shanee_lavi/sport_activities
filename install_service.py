#!/usr/bin/env python

import shutil
import subprocess
import os

SERVICE_FILE_NAME = 'tlv_act.service'


def create_service():
    print 'Setup Service'

    def systemctl_command(command):
        """
        :param str command:
        :rtype: list
        """
        base_command = ['systemctl']
        base_command.extend(command.split())

        return base_command

    # Copy service file
    shutil.copy(SERVICE_FILE_NAME, '/etc/systemd/system/' + SERVICE_FILE_NAME)
    # Start service
    subprocess.call(systemctl_command('daemon-reload'))
    subprocess.call(systemctl_command('enable tlv_act.service'))
    subprocess.call(systemctl_command('start tlv_act.service'))
    # Check service
    output = subprocess.check_output(systemctl_command('status tlv_act.service'))

    if output.find('Active: active (running)'):
        print 'Service up and running'
    else:
        print 'Service failed'


if __name__ == '__main__':
    if os.getuid() == 0:
        create_service()
    else:
        print("Please run as root. Sorry.")
